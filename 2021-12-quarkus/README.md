# Quarkus

## Status

| Date       | Who     | Comment                |
|:-----------|:--------|:-----------------------|
| 2021-12-28 | Nobby   | Started                |

## Resources

Official pages:

* https://quarkus.io/
* https://code.quarkus.io/
* https://quarkus.io/guides/

Other resources:

* https://quarkus.io/guides/hibernate-orm-panache
* https://quarkus.io/guides/hibernate-orm-panache#simplified-queries
* https://docs.jboss.org/hibernate/orm/5.4/userguide/html_single/Hibernate_User_Guide.html#hql
* https://docs.jboss.org/hibernate/core/3.5/reference/en/html/queryhql.html#queryhql-select

## Tips & Tricks

### Controllers

```java
// Return 201 (created) response from a controller (web resource)
return Response.created(URI.create("/persons/" + person.id)).build();

// Throwing an exception in controller when entity could not be found
Person person = resource.get(id);
if (person == null) {
	throw new WebApplicationException(404);
}

// Get endpoint where the sort has been specified as a query parameter
@GET
@Produces("application/json")
public Response list(@QueryParam("sort") List<String> sortQuery,
		@QueryParam("page") @DefaultValue("0") int pageIndex,
		@QueryParam("size") @DefaultValue("20") int pageSize) {
	Page page = Page.of(pageIndex, pageSize);
	Sort sort = getSortFromQuery(sortQuery);
	List<Person> people = resource.list(page, sort);
	// ... build a response with page links and return a 200 response with a list
}

// Update if entity found, else insert
@Transactional
@PUT
@Path("{id}")
@Consumes("application/json")
@Produces("application/json")
public Response update(@PathParam("id") Long id, Person personToSave) {
	if (resource.get(id) == null) {
		Person person = resource.update(id, personToSave);
		return Response.status(204).build();
	}
	Person person = resource.update(id, personToSave);
	// ... build a new location URL and return 201 response with an entity
}

// Delete a resource, throwing when not found
@Transactional
@DELETE
@Path("{id}")
public void delete(@PathParam("id") Long id) {
	if (!resource.delete(id)) {
		throw new WebApplicationException(404);
	}
}
```

### Panache

```java
// Various way to sort with Panache
List<Person> persons = Person.list("order by name,birth");
List<Person> persons = Person.list(Sort.by("name").and("birth"));
List<Person> persons = Person.list("status", Sort.by("name").and("birth"), Status.Alive);

// Maybe use named queries when things get hairy
@Entity
@NamedQueries({
    @NamedQuery(name = "Person.getByName", query = "from Person where name = ?1"),
    @NamedQuery(name = "Person.countByStatus", query = "select count(*) from Person p where p.status = :status"),
    @NamedQuery(name = "Person.updateStatusById", query = "update Person p set p.status = :status where p.id = :id"),
    @NamedQuery(name = "Person.deleteById", query = "delete from Person p where p.id = ?1")
})
public class Person extends PanacheEntity {
}
```
