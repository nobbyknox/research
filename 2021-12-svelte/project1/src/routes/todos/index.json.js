import { api } from './_api';

// GET /todos.json
export const get = async (request) => {

	console.log('Top of index.json.js::get');
	console.log('  request.locals.userid: ' + request.locals.userid);

	// request.locals.userid comes from src/hooks.js
	const response = await api(request, `todos/${request.locals.userid}`);

	if (response.status === 404) {
		// user hasn't created a todo list.
		// start with an empty array
		return { body: [] };
	}

	return response;
};

// POST /todos.json
export const post = async (request) => {

	console.log('Top of index.json.js::post');

	const response = await api(request, `todos/${request.locals.userid}`, {
		// because index.svelte posts a FormData object,
		// request.body is _also_ a (readonly) FormData
		// object, which allows us to get form data
		// with the `body.get(key)` method
		text: request.body.get('text')
	});

	return response;
};
