# Svelte and SvelteKit

## Status

| Date       | Who     | Comment                |
|:-----------|:--------|:-----------------------|
| 2021-12-12 | Nobby   | Started                |

## Resources

### Official

* https://svelte.dev
* https://kit.svelte.dev
* https://svelte.dev/blog

### Blogs

* https://www.sitepoint.com/a-beginners-guide-to-sveltekit/

### YouTube

* https://www.youtube.com/playlist?list=PLm_Qt4aKpfKi7J6Jc2OR7YLz1hEhC_4Mc

### Udemy

* https://www.udemy.com/course/introduction-to-svelte-and-sveltekit/
* https://www.udemy.com/course/sveltejs-the-complete-guide/		<- older, and uses rollup instead of sveltekit

## Notes

Some initial impressions:

* `.svelte` files are client-side, while `.js|.ts` files are server-side endpoints.

